import common
import os
import numpy as np

if __name__ == '__main__':

    start, stop, npoints = -20, -100, 10
    path = './data/ramp_meas/{}'.format(common.timestamp())
    os.makedirs(path)

    voltages = np.linspace(start, stop, num=npoints)
    currents = np.zeros_like(voltages)
    sourcemeter = common.init()

    for i in range(npoints):
        v = voltages[i]
        currents[i] = common.measure(sourcemeter, v, delay=1)
    common.ramp(sourcemeter, stop, 1, 10) # cool down

    common.save(path, voltages, currents)
    common.plot(path, voltages, currents)
    sourcemeter.shutdown()
