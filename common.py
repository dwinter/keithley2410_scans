import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from time import sleep
from datetime import datetime
import sys
import csv

from PrologixEthernetAdapter import PrologixEthernetAdapter
from pymeasure.instruments.keithley import Keithley2400

def init():
    adapter = PrologixEthernetAdapter('128.141.89.142', address=4)
    sourcemeter = Keithley2400(adapter)
    sourcemeter.reset()
    sourcemeter.use_front_terminals()
    sourcemeter.measure_current()
    sourcemeter.apply_voltage()
    sourcemeter.source_voltage = 0
    sourcemeter.enable_source()
    sleep(0.1) # wait here to give the instrument time to react
    return sourcemeter

def ramp(sourcemeter, start, stop, step):
    for v in range(start, stop, step):
        sourcemeter.source_voltage = v
        sleep(1)
    return sourcemeter

def measure(sourcemeter, voltage, delay=0):
    sourcemeter.source_voltage = voltage
    sleep(delay)
    return sourcemeter.current

def timestamp():
    return datetime.now().strftime('%Y%m%d_%H%M%S')

def plot(path, voltages, currents):
    xs, ys = np.absolute(voltages), np.absolute(currents)
    xs *= 1e6   # A -> uA
    plt.plot(xs, ys, 'bo-')
    plt.xlabel(r'-$V_{bias}$ ($V$)')
    plt.ylabel(r'-$I_{leak}$ ($\mu{A}$)')
    plt.yscale('log')
    plt.title('Leakage current vs bias voltage')
    # path = './iv_module{}_{}.png'.format(module_id, timestamp()) #810
    plt.savefig(path+'/iv.png')
    return path

def save(path, voltages, currents):
    with open(path+'/iv.csv', 'a') as fd:
        writer = csv.writer(fd)
        for v, i in zip(voltages, currents):
            writer.writerow([v, i])
    return path

def exit_handler(sourcemeter, start, sig, fname):
    print("Ctrl-C pressed. Cool down started. Don't interrupt.")
    ramp(sourcemeter, start, 1, 10)
    print("Cool down complete. Shutdown.")
    sourcemeter.shutdown()
    sys.exit(0)
