import common
import signal
from functools import partial
import os

if __name__ == '__main__':

    #v = -300
    v = -50
    path = './data/cont_meas/{}'.format(common.timestamp())
    os.makedirs(path)

    sourcemeter = common.init()
    signal.signal(signal.SIGINT, partial(common.exit_handler, sourcemeter, v))
    common.ramp(sourcemeter, 0, v, -10) # heat up
    while(True):
        i = common.measure(sourcemeter, v, delay=5)
        common.save(path, [v], [i])
